                           **INSTRUCTIONS**
                           
                           
1.   Run ChessPieceDemo class

2.   You should insert:
*    The maximum count of moves
*    The type of chess piece (only type knight is supported , so press **knight**)
*    The starting row
*    The starting column
*    The destination row
*    The destination column
    

                           **NOTES**

*  If you set as chesspiece type other than knight, "Invalid chess piece" will be
appeared

*  If you set same cell coordinates for start and destination point ,
"Wrong input, invalid starting or destination cell. Please try again" will be 
appeared 

*  If you set, out of chess board boundaries, cell coordinates for start or destination point ,
"Wrong input, invalid starting or destination cell. Please try again" will be 
appeared

*  If the destination and start cell coordinates are valid but cannot find for the 
given count of moves , "No available path" will be appeared

* If the destination and start cell coordinates are valid and path is found for the 
given count of moves , the path will be appeared (e.g. "The complete path is->( row:3 - column:3 )->( row:1 - column:2 )")

*  ChessBoard size is 8x8




