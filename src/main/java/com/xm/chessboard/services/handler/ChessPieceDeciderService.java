package com.xm.chessboard.services.handler;

import com.xm.chessboard.model.chess.ChessCell;
import com.xm.chessboard.services.route.calculator.ChessBoardRouteCalculator;
import lombok.RequiredArgsConstructor;

import static com.xm.chessboard.model.chess.ChessPieceType.KNIGHT;

@RequiredArgsConstructor
public class ChessPieceDeciderService implements ChessPieceDecider {

    private final int maxMoves;
    private final ChessBoardRouteCalculator chessBoardRouteCalculator;

    @Override
    public String detectChessPieceType(String type, ChessCell startingCell, ChessCell destinationCell) {
        if (type.equals(KNIGHT.getChessPieceType())) {
            return chessBoardRouteCalculator.provideShortestRoute(startingCell, destinationCell, maxMoves);
        } else {
            return "Invalid chess piece";
        }
    }
}
