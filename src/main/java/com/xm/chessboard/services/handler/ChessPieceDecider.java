package com.xm.chessboard.services.handler;

import com.xm.chessboard.model.chess.ChessCell;

public interface ChessPieceDecider {

    String detectChessPieceType(String type, ChessCell startingCell, ChessCell destinationCell);
}
