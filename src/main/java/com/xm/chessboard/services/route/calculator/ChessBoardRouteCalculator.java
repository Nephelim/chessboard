package com.xm.chessboard.services.route.calculator;

import com.xm.chessboard.model.chess.ChessCell;

public interface ChessBoardRouteCalculator {

    String provideShortestRoute(ChessCell startingCell, ChessCell destinationCell, int maxMoves);

}
