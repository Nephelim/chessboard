package com.xm.chessboard.services.route.calculator;

import com.xm.chessboard.model.chess.ChessCell;
import com.xm.chessboard.model.chess.Knight;
import com.xm.chessboard.model.moves.ChessCellNode;
import com.xm.chessboard.model.moves.KnightMovement;
import com.xm.chessboard.services.validation.ChessBoardMoveValidator;
import com.xm.chessboard.utils.ChessPieceRouteUtils;
import lombok.RequiredArgsConstructor;

import java.util.ArrayDeque;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ChessBoardKnightRouteCalculator implements ChessBoardRouteCalculator {

    private final ChessBoardMoveValidator<KnightMovement> chessBoardKnightMoveValidator;

    @Override
    public String provideShortestRoute(ChessCell startingCell, ChessCell destinationCell, int maxMoves) {
        KnightMovement knightMovement = constructKnightMovement(startingCell, destinationCell, new LinkedHashSet<>());
        if (chessBoardKnightMoveValidator.areNotValidInputCells(knightMovement)) {
            return "Wrong input, invalid starting or destination cell. Please try again";
        } else {
            Optional<ChessCellNode> chessCellNodeOpt = calculateShortestRoute(knightMovement, maxMoves);
            return chessCellNodeOpt.isPresent() ? ChessPieceRouteUtils.constructRoute(chessCellNodeOpt.get()) : "No available path";
        }
    }

    private Optional<ChessCellNode> calculateShortestRoute(KnightMovement knightMovement, int maxMoves) {
        Queue<ChessCellNode> availableMoves = new ArrayDeque<>();
        availableMoves.add(ChessCellNode.builder().currentCell(knightMovement.getKnight().getCurrentLocation()).distance(0).build());
        while (!availableMoves.isEmpty()) {
            ChessCellNode knightCurrentLocation = availableMoves.poll();
            Set<ChessCellNode> updatedVisitedChessCells = updateVisitedCells(knightMovement.getVisitedCells(), knightCurrentLocation);
            knightMovement = constructKnightMovement(knightCurrentLocation.getCurrentCell(), knightMovement.getDestinationCell(), updatedVisitedChessCells);
            if (chessBoardKnightMoveValidator.isSameCell(knightCurrentLocation.getCurrentCell(), knightMovement.getDestinationCell())) {
                return Optional.of(knightCurrentLocation);
            } else {
                availableMoves.addAll(calculateNextChestCell(knightCurrentLocation, knightMovement, maxMoves));
            }
        }
        return Optional.empty();
    }

    private Queue<ChessCellNode> calculateNextChestCell(ChessCellNode knightCurrentLocation, KnightMovement knightMovement, int maxMoves) {
        return knightMovement.getKnightMoves().stream()
                .map(availableKnightMoves(knightCurrentLocation))
                .filter(validKnightNextMove(knightMovement.getVisitedCells(), maxMoves))
                .collect(Collectors.toCollection(ArrayDeque::new));
    }

    private Predicate<ChessCellNode> validKnightNextMove(Set<ChessCellNode> visitedCells, int maxMoves) {
        return nextChessBoardCell -> (nextChessBoardCell.getDistance() <= maxMoves && (isNotVisitedChessCell(visitedCells, nextChessBoardCell)
                && !chessBoardKnightMoveValidator.areNotValidCellCoordinates(nextChessBoardCell.getCurrentCell())));
    }

    private ChessCell calculateCellCoordinates(ChessCell nextChessCell, ChessCell prevChessCell) {
        return ChessCell.builder()
                .column(prevChessCell.getColumn() + nextChessCell.getColumn())
                .row(prevChessCell.getRow() + nextChessCell.getRow())
                .build();
    }

    private Function<ChessCell, ChessCellNode> availableKnightMoves(ChessCellNode knightCurrentLocation) {
        return nextChessBoardCell -> ChessCellNode.builder()
                .currentCell(calculateCellCoordinates(nextChessBoardCell, knightCurrentLocation.getCurrentCell()))
                .distance(knightCurrentLocation.getDistance() + 1)
                .parent(knightCurrentLocation)
                .build();
    }

    private Set<ChessCellNode> updateVisitedCells(Set<ChessCellNode> visitedCells, ChessCellNode chessBoardCell) {
        visitedCells.add(chessBoardCell);
        return visitedCells;
    }

    private boolean isNotVisitedChessCell(Set<ChessCellNode> visitedCells, ChessCellNode chessBoardCell) {
        return !visitedCells.contains(chessBoardCell);
    }

    private Knight constructKnight(ChessCell startingCell) {
        return Knight.builder().currentLocation(startingCell).build();
    }

    private KnightMovement constructKnightMovement(ChessCell startingCell, ChessCell destinationCell, Set<ChessCellNode> visitedCells) {
        return KnightMovement.builder()
                .knight(constructKnight(startingCell))
                .destinationCell(destinationCell)
                .visitedCells(visitedCells)
                .build();
    }

}
