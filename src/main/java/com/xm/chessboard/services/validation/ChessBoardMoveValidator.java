package com.xm.chessboard.services.validation;

import com.xm.chessboard.model.chess.ChessCell;

public interface ChessBoardMoveValidator<T> {


    boolean areNotValidInputCells(T chessPieceMovement);

    boolean areNotValidCellCoordinates(ChessCell cell);

    boolean isSameCell(ChessCell startingCell, ChessCell destinationCell);

    boolean isOutOfChessBoardCoordinates(Integer row, Integer column);

}
