package com.xm.chessboard.services.validation;

import com.xm.chessboard.model.chess.ChessBoard;
import com.xm.chessboard.model.chess.ChessCell;
import com.xm.chessboard.model.moves.KnightMovement;


public class ChessBoardKnightMoveValidator implements ChessBoardMoveValidator<KnightMovement> {

    @Override
    public boolean areNotValidInputCells(KnightMovement knightMovement) {
        ChessCell startingCell = knightMovement.getKnight().getCurrentLocation();
        ChessCell destinationCell = knightMovement.getDestinationCell();
        return isSameCell(startingCell, destinationCell) || areNotValidCellCoordinates(startingCell) || areNotValidCellCoordinates(destinationCell);
    }

    @Override
    public boolean isOutOfChessBoardCoordinates(Integer row, Integer column) {
        return row < ChessBoard.MIN_SIZE.getSize() || column < ChessBoard.MIN_SIZE.getSize() ||
                row >= ChessBoard.MAX_SIZE.getSize() || column >= ChessBoard.MAX_SIZE.getSize();
    }

    @Override
    public boolean areNotValidCellCoordinates(ChessCell chessCell) {
        return isOutOfChessBoardCoordinates(chessCell.getRow(), chessCell.getColumn());

    }

    @Override
    public boolean isSameCell(ChessCell startingCell, ChessCell destinationCell) {
        return startingCell.equals(destinationCell);
    }


}
