package com.xm.chessboard.model.moves;

import com.xm.chessboard.model.chess.ChessCell;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor
@Builder(toBuilder = true)
public class ChessCellNode {

    ChessCell currentCell;
    Integer distance;
    ChessCellNode parent;

}
