package com.xm.chessboard.model.moves;

import com.xm.chessboard.model.chess.ChessCell;
import com.xm.chessboard.model.chess.Knight;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Value
@RequiredArgsConstructor
public class KnightMovement {

    Knight knight;
    ChessCell destinationCell;
    Set<ChessCellNode> visitedCells;
    List<ChessCell> knightMoves;

    @Builder
    public KnightMovement(Knight knight, ChessCell destinationCell, Set<ChessCellNode> visitedCells) {
        this.knight = knight;
        this.destinationCell = destinationCell;
        this.visitedCells = visitedCells;
        this.knightMoves = constructKnightMoves();
    }

    private List<ChessCell> constructKnightMoves() {
        return Arrays.asList(
                ChessCell.builder().column(2).row(-1).build(),
                ChessCell.builder().column(2).row(1).build(),
                ChessCell.builder().column(-2).row(1).build(),
                ChessCell.builder().column(-2).row(-1).build(),
                ChessCell.builder().column(1).row(2).build(),
                ChessCell.builder().column(1).row(-2).build(),
                ChessCell.builder().column(-1).row(2).build(),
                ChessCell.builder().column(-1).row(-2).build());
    }
}
