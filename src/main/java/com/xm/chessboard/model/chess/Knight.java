package com.xm.chessboard.model.chess;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder(toBuilder = true)
public class Knight {

    private ChessCell currentLocation;

}
