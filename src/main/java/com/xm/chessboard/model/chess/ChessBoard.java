package com.xm.chessboard.model.chess;

public enum ChessBoard {
    MAX_SIZE(8),
    MIN_SIZE(0);

    private Integer size;

    ChessBoard(Integer size) {
        this.size = size;
    }

    public Integer getSize() {
        return size;
    }
}
