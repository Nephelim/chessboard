package com.xm.chessboard.model.chess;


import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Data
@Value
@Builder(toBuilder = true)
public class ChessCell {
    Integer row;
    Integer column;
}
