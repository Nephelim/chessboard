package com.xm.chessboard.model.chess;

public enum ChessPieceType {
    KNIGHT("knight");

    private String chessPieceType;

    ChessPieceType(String type) {
        this.chessPieceType = type;
    }

    public String getChessPieceType() {
        return chessPieceType;
    }
}
