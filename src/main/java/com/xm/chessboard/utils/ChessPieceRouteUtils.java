package com.xm.chessboard.utils;

import com.xm.chessboard.model.moves.ChessCellNode;

public class ChessPieceRouteUtils {

    private static String ROUTE;

    public static String constructRoute(ChessCellNode visitedCell) {
        ROUTE = "The complete path is";
        if (visitedCell.getParent() != null) {
            constructRoute(visitedCell.getParent());
        }
        ROUTE += joinRouteNodes(visitedCell.getCurrentCell().getRow(), visitedCell.getCurrentCell().getColumn());
        return ROUTE;
    }

    private static String joinRouteNodes(int row, int column) {
        return new StringBuilder("->( row:")
                .append(row)
                .append(" - column:")
                .append(column)
                .append(" )")
                .toString();
    }
}
