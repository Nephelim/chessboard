import com.xm.chessboard.model.chess.ChessCell;
import com.xm.chessboard.services.handler.ChessPieceDeciderService;
import com.xm.chessboard.services.route.calculator.ChessBoardKnightRouteCalculator;
import com.xm.chessboard.services.validation.ChessBoardKnightMoveValidator;

import java.util.Scanner;

public class ChessPieceDemo {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter type of chess piece. ( Dont forget: knight ;) )");
        String type = in.nextLine();
        System.out.println("Please enter the maximum moves ");
        int maxMoves = in.nextInt();
        System.out.println("Please enter the starting row ");
        int currentRow = in.nextInt();
        System.out.println("Please enter the starting column ");
        int currentColumn = in.nextInt();
        System.out.println("Please enter the destination row ");
        int destinationRow = in.nextInt();
        System.out.println("Please enter the destination column ");
        int destinationColumn = in.nextInt();

        ChessPieceDeciderService chessPieceDecider = new ChessPieceDeciderService(maxMoves,
                new ChessBoardKnightRouteCalculator(new ChessBoardKnightMoveValidator()));
        ChessCell startingCell = ChessCell.builder().row(currentRow).column(currentColumn).build();
        ChessCell destinationCell = ChessCell.builder().row(destinationRow).column(destinationColumn).build();
        System.out.print(chessPieceDecider.detectChessPieceType(type, startingCell, destinationCell));

    }
}
