package utils

import com.xm.chessboard.model.chess.ChessCell
import com.xm.chessboard.model.chess.Knight

trait KnightUtils {

    Knight getKnight(ChessCell cell) {
        Knight.builder()
                .currentLocation(cell)
                .build()
    }


}