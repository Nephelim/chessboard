package utils

import com.xm.chessboard.model.chess.ChessCell

trait ChessCellUtils {

    ChessCell INVALID_CELL = getChessCell(-1, -1)
    ChessCell VALID_CELL_MIN = getChessCell(0, 0)

    ChessCell getChessCell(Integer row, Integer column) {
        ChessCell.builder()
                .column(column)
                .row(row)
                .build()
    }
}
