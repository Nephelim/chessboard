package utils

import com.xm.chessboard.model.chess.ChessCell
import com.xm.chessboard.model.moves.ChessCellNode

trait ChessCellNodeUtils implements ChessCellUtils {

    ChessCellNode ROOT_NODE = getChessCellNode(getChessCell(0,0), null)
    ChessCellNode NODE_1 = getChessCellNode(getChessCell(1, 1), ROOT_NODE)
    ChessCellNode NODE_2 = getChessCellNode(getChessCell(2, 2), NODE_1)


    ChessCellNode getChessCellNode(ChessCell cell, ChessCellNode cellNode) {
        ChessCellNode.builder()
                .currentCell(cell)
                .parent(cellNode)
                .build()
    }
}
