package utils

import com.xm.chessboard.model.chess.ChessCell
import com.xm.chessboard.model.moves.KnightMovement

trait KnightMovementUtils implements KnightUtils {

    KnightMovement getKnightMovement(ChessCell knightCell, ChessCell destinationCell) {
        KnightMovement.builder()
                .knight(getKnight(knightCell))
                .destinationCell(destinationCell)
                .build()
    }

}