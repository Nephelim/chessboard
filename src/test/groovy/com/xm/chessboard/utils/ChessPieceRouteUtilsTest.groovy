package com.xm.chessboard.utils


import spock.lang.Specification
import spock.lang.Unroll
import utils.ChessCellNodeUtils

class ChessPieceRouteUtilsTest extends Specification implements ChessCellNodeUtils {

    ChessPieceRouteUtils chessPieceRouteUtils

    void setup() {
        chessPieceRouteUtils = new ChessPieceRouteUtils()
    }


    @Unroll
    def "check path for cell with #testCase"() {
        when:
        def result = chessPieceRouteUtils.constructRoute(node)
        then:
        result == invalid
        where:
        testCase               | node      | invalid
        "with 2 parent nodes " | NODE_2    | 'The complete path is->( row:0 - column:0 )->( row:1 - column:1 )->( row:2 - column:2 )'
        "with 1 parent node "  | NODE_1    | 'The complete path is->( row:0 - column:0 )->( row:1 - column:1 )'
        "only parent node "    | ROOT_NODE | 'The complete path is->( row:0 - column:0 )'
    }
}
