package com.xm.chessboard.services.handler


import com.xm.chessboard.services.route.calculator.ChessBoardRouteCalculator
import spock.lang.Specification
import utils.ChessCellUtils

class ChessPieceDeciderServiceTest extends Specification implements ChessCellUtils {

    static final int MAX_MOVES = 3

    private ChessBoardRouteCalculator chessBoardKnightMoveValidator
    private ChessPieceDecider chessPieceDecider

    def STARTING_CELL = getChessCell(0, 0)
    def DESTINATION_CELL = getChessCell(7, 7)
    def INV_CHESS_TYPE = "king"
    def CHESS_TYPE = "knight"

    void setup() {
        chessBoardKnightMoveValidator = Mock(ChessBoardRouteCalculator)
        chessPieceDecider = new ChessPieceDeciderService(MAX_MOVES, chessBoardKnightMoveValidator)
    }


    def "check invocation for chess type knight "() {
        when:
        chessPieceDecider.detectChessPieceType(CHESS_TYPE, STARTING_CELL, DESTINATION_CELL)
        then:
        1 * chessBoardKnightMoveValidator.provideShortestRoute(STARTING_CELL, DESTINATION_CELL, MAX_MOVES)
    }

    def "check invocation for chess type invalid "() {
        when:
        chessPieceDecider.detectChessPieceType(INV_CHESS_TYPE, STARTING_CELL, DESTINATION_CELL)
        then:
        0 * chessBoardKnightMoveValidator.provideShortestRoute(STARTING_CELL, DESTINATION_CELL, MAX_MOVES)
    }

    def "check result for chess type knight "() {
        def message = "No available path"
        given:
        chessBoardKnightMoveValidator.provideShortestRoute(STARTING_CELL, DESTINATION_CELL, MAX_MOVES) >> message
        when:
        def result = chessPieceDecider.detectChessPieceType(CHESS_TYPE, STARTING_CELL, DESTINATION_CELL)
        then:
        result == message
    }

    def "check result for chess type invalid "() {
        def message = "Invalid chess piece"
        when:
        def result = chessPieceDecider.detectChessPieceType(INV_CHESS_TYPE, STARTING_CELL, DESTINATION_CELL)
        then:
        result == message
    }


}
