package com.xm.chessboard.services.validation

import com.xm.chessboard.model.moves.KnightMovement
import spock.lang.Specification
import spock.lang.Unroll
import utils.ChessCellUtils
import utils.KnightMovementUtils

class ChessBoardKnightMoveValidatorTest extends Specification implements ChessCellUtils, KnightMovementUtils {

    private ChessBoardMoveValidator<KnightMovement> chessBoardKnightMoveValidator

    void setup() {
        chessBoardKnightMoveValidator = new ChessBoardKnightMoveValidator()
    }

    @Unroll
    def "check if are not valid input cells #testCase"() {
        when:
        def result = chessBoardKnightMoveValidator.areNotValidInputCells(knightMovement)
        then:
        result == invalid
        where:
        testCase                    | knightMovement                                        | invalid
        "same input cells "         | getKnightMovement(VALID_CELL_MIN, VALID_CELL_MIN)     | true
        "invalid destination cell " | getKnightMovement(VALID_CELL_MIN, INVALID_CELL)       | true
        "invalid starting cell "    | getKnightMovement(INVALID_CELL, VALID_CELL_MIN)       | true
        "invalid both input cells " | getKnightMovement(INVALID_CELL, INVALID_CELL)         | true
        "valid both input cells "   | getKnightMovement(getChessCell(5, 5), VALID_CELL_MIN) | false

    }

    @Unroll
    def "check if  same #testCase"() {
        when:
        def result = chessBoardKnightMoveValidator.isSameCell(cell1, cell2)
        then:
        result == same
        where:
        testCase                | cell1          | cell2          | same
        "same input cells "     | VALID_CELL_MIN | VALID_CELL_MIN | true
        "not same input cells " | VALID_CELL_MIN | INVALID_CELL   | false
    }

    @Unroll
    def "check if cell coordinates  #testCase"() {
        when:
        def result = chessBoardKnightMoveValidator.areNotValidCellCoordinates(cell)
        then:
        result == valid
        where:
        testCase                     | cell           | valid
        "valid cell coordinates "    | VALID_CELL_MIN | false
        "invalid cell coordinates  " | INVALID_CELL   | true
    }

    @Unroll
    def "check chessBoard size  #testCase"() {
        when:
        def result = chessBoardKnightMoveValidator.isOutOfChessBoardCoordinates(column, row)
        then:
        result == valid
        where:
        testCase               | column | row | valid
        "invalid column size " | -1     | 1   | true
        "invalid row size  "   | 1      | -1  | true
        "both invalid   "      | -1     | -1  | true
        "both valid   "        | 1      | 1   | false
    }

}
