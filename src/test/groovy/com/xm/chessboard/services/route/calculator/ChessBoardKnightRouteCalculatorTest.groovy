package com.xm.chessboard.services.route.calculator

import com.xm.chessboard.model.moves.KnightMovement
import com.xm.chessboard.services.validation.ChessBoardKnightMoveValidator
import com.xm.chessboard.services.validation.ChessBoardMoveValidator
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import utils.ChessCellUtils

class ChessBoardKnightRouteCalculatorTest extends Specification implements ChessCellUtils {

    private ChessBoardMoveValidator<KnightMovement> chessBoardKnightMoveValidator
    private ChessBoardRouteCalculator chessBoardKnightRouteCalculator

    @Shared
    def INVALID_POINT_MESSAGE = "Wrong input, invalid starting or destination cell. Please try again"
    @Shared
    def MAX_MOVES_3 = 3

    @Shared
    def NO_PATH = "No available path"


    void setup() {
        chessBoardKnightMoveValidator = new ChessBoardKnightMoveValidator()
        chessBoardKnightRouteCalculator = new ChessBoardKnightRouteCalculator(chessBoardKnightMoveValidator)
    }

    @Unroll
    def "get route for knight when is #testCase  "() {
        when:
        def result = chessBoardKnightRouteCalculator.provideShortestRoute(sourceCell, destinationCell, MAX_MOVES_3)
        then:
        result == message
        where:
        testCase                                | sourceCell     | destinationCell | message
        "invalid source cell "                  | INVALID_CELL   | VALID_CELL_MIN  | INVALID_POINT_MESSAGE
        "invalid destination cell  "            | VALID_CELL_MIN | INVALID_CELL    | INVALID_POINT_MESSAGE
        "invalid destination and source cell  " | INVALID_CELL   | INVALID_CELL    | INVALID_POINT_MESSAGE


    }

    @Unroll
    def "get route for knight when  #testCase for #moves "() {
        when:
        def result = chessBoardKnightRouteCalculator.provideShortestRoute(sourceCell, destinationCell, totalMoves)
        then:
        result == message
        where:
        testCase                                | sourceCell         | destinationCell    | totalMoves | message
        "path exists "                          | getChessCell(3, 3) | getChessCell(1, 2) | 3          | "The complete path is->( row:3 - column:3 )->( row:1 - column:2 )"
        "path exists "                          | getChessCell(3, 3) | getChessCell(1, 2) | 2          | "The complete path is->( row:3 - column:3 )->( row:1 - column:2 )"
        "path dont exists "                     | getChessCell(5, 5) | getChessCell(1, 1) | 3          | NO_PATH
        "path exists "                          | getChessCell(5, 5) | getChessCell(1, 1) | 10         | "The complete path is->( row:5 - column:5 )->( row:4 - column:7 )->( row:3 - column:5 )->( row:2 - column:3 )->( row:1 - column:1 )"
        "destination and source cell are same " | getChessCell(5, 5) | getChessCell(5, 5) | 3          | INVALID_POINT_MESSAGE


    }
}
